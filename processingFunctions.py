import numpy as np
from scipy import signal
from scipy.fftpack import fft, fftshift
from scipy.signal import windows
import matplotlib.pyplot as plt
import itertools as it
# import cv2


#def delete_duplicate_pairs(*arrays):
#    unique = set()
#    arrays = list(arrays)
#    n = range(len(arrays))
#    index = 0
#    for pair in it.izip(*arrays):
#        if pair not in unique:
#            unique.add(pair)
#            for i in n:
#                arrays[i][index] = pair[i]
#            index += 1
#    return [a[:index] for a in arrays]

def rvMapProc(input_cube, prms):
    """
    расчёт двумерной карты "дальность-скорость".

    Для каждого виртуального приёмника выполняются:
        1. преобразование Фурье вдоль оси отсчётов для каждого импульса;
        2. преобразование Фурье вдоль оси импульсов для каждого отсчёта.

    Полученные после преобразований Фурье двумерные карты
    в количестве виртуальных приёмников складываются, т.о. строится
    суммарная карта "дальность-скорость".

    Входные параметры:
        1. input_cube - входной куб сырых радарных данных
        2. prms - массив размеров входной и возвращаемой карт,
           prms['smp_num'] - количество отсчётов в импульсах
           prms['pls_num'] - количество импульсов
           prms['rbins_num'] - количество отсчётов по дальности
           prms['dbins_num'] - количество отсчётов по доплеру

    Возвращаемые параметры:
        rvmap_sum - суммарная RV-карта
        RVcube - куб данных после выполнения двумерного преобразования \
                 Фурье на каждом виртуальном приёмнике
    """
    smp_num = prms['smp_num']
    pls_num = prms['pls_num']
    rbins_num = prms['rbins_num']
    dbins_num = prms['dbins_num']

    # Выполнение FFT по дальности для MIMO субкадра
    hamwin_range = signal.hamming(smp_num)
    hamwin_vel = signal.hamming(pls_num)

    rvmap = fft(input_cube[:, :, :]*hamwin_range[None, :, None],
                n=rbins_num, axis=1)

    # Выполнение FFT по скорости для MIMO субкадра

    rvmap = fft(rvmap*hamwin_vel[:, None, None], n=dbins_num, axis=0)

    rvmap = fftshift(rvmap, 0)
    
    rvmap = rvmap.real**2 + rvmap.imag**2

    rvmap_sum = np.sum(rvmap, axis=2)

    return rvmap_sum, rvmap


def deg2rad(x):
    return x * np.pi / 180


def rad2deg(x):
    return x * 180 / np.pi


def raMapBuilding(input_cube, prms):
    """
    расчёт двумерной карты "дальность-азимут".

    Для каждого импульса выполняются:
        1. преобразование Фурье вдоль оси отсчётов внутри импульса;
        2. преобразование Фурье вдоль оси виртуальных приёмников.

    Полученные после преобразований Фурье двумерные карты
    в количестве импульсов складываются, т.о. строится
    суммарная карта "дальность-азимут".

    Входные параметры:
        1. input_cube - входной куб сырых радарных данных
        2. prms - массив размеров входной и возвращаемой карт,
           prms['smp_num'] - количество отсчётов в импульсах
           prms['rxq_num'] - количество виртуальных приёмников
           prms['rbins_num'] - количество отсчётов по дальности
           prms['abins_num'] - количество отсчётов по азимуту

    Возвращаемые параметры:
        ramap_sum - суммарная RV-карта
        RAcube - куб данных после выполнения двумерного преобразования \
                 Фурье на каждом виртуальном приёмнике
    """

    c = 299792458.0
    f = 77e9
    lmb = c/f
    d = lmb/2

    f_ang = np.fft.fftfreq(2048, d)
    angles = np.rad2deg(np.arcsin(f_ang * lmb))
    angles = np.fft.fftshift(angles[:]*1)

    smp_num = prms['smp_num']
    rxq_num = prms['rxq_num']
    rbins_num = prms['rbins_num']
    abins_num = prms['abins_num']
    rbeta = prms['range_beta']
    abeta = prms['azims_beta']

    # Выполнение FFT по дальности для MIMO субкадра
    win_range = signal.hamming(smp_num)
    win_azim = signal.hamming(rxq_num)
    plots = False

    if (plots):
        ########################
        plt.figure()
        plt.title("Окно Хэмминга, (используется для дальностей)")
        plt.plot(win_range)

        ########################
        plt.figure()
        plt.title("АЧХ окна Хэмминга, (используется для дальностей)")
        plt.plot(angles, 10*np.log10(fftshift(abs(fft(win_range, 2048)))))
        plt.xlim([-85, 85])
        plt.ylim([-70, 21.5])
        plt.xlabel('angle [degrees]')
        plt.ylabel('signal')
        plt.xlabel('angle [rad]')

        ########################
        plt.figure()
        plt.title("Окно Хэмминга, (используется для азимутов)")
        plt.plot(win_azim)
        ########################

        plt.figure()
        plt.title("АЧХ окна Хэмминга, (используется для азимутов)")
        plot_mas = 10*np.log10(fftshift(abs(fft(win_azim, 2048))))
        plt.plot(angles, plot_mas)
        # plt.axhline(y=0.707*np.max(plot_mas), color='b', linestyle='--',
        #             lw = 1)
        plt.xlim([-85, 85])
        plt.ylim([-70, 16])
        plt.xlabel('angle [degrees]')
        plt.ylabel('signal')
        plt.xlabel('angle [rad]')

    ######################################################################
#    win_range = windows.kaiser(smp_num, beta=rbeta)
#    win_azim = windows.kaiser(rxq_num, beta=abeta)

    if (plots):
        plt.figure()
        plt.plot(win_range)
        plt.title("Окно Кайзера при Б, равном 10 (используем для дальностей)")

        #########################
        plt.figure()
        plt.title("АЧХ окна Кайзера при Б, равном 10 (256 входных отсчёта)")
        plt.plot(angles, 10*np.log10(fftshift(abs(fft(win_range, 2048)))))
        plt.xlim([-85, 85])
        plt.ylim([-70, 21.5])
        plt.xlabel('angle [degrees]')
        plt.ylabel('signal')
        plt.xlabel('angle [rad]')

        #########################
        plt.figure()
        plt.plot(win_azim)
        plt.title("Окно Кайзера при Б, равном 20 (используется для азимутов)")

        #########################
        plt.figure()
        plot_mas = 10*np.log10(fftshift(abs(fft(windows.kaiser(rxq_num, 15),
                                                2048))))
        plt.plot(angles, plot_mas)
        # plt.axhline(y=0.707*np.max(plot_mas), color='r', linestyle='--',
        #             lw = 1)
        plt.title("АЧХ окна Кайзера при Бета, равном 20 (64 входных отсчёта)")
        plt.xlim([-85, 85])
        plt.ylim([-70, 16])
        plt.xlabel('angle [degrees]')
        plt.ylabel('signal')
        plt.xlabel('angle [rad]')

    # win_range = windows.blackmanharris(smp_num)
    # win_azim = windows.blackmanharris(rxq_num)

    # win_range = windows.blackman(smp_num)
    # win_azim = windows.blackman(rxq_num)

    RAmap = fft(input_cube*win_range[None, :, None],
                n=rbins_num, axis=1)

    # Выполнение FFT по азимуту для MIMO субкадра

    RAmap = fft(RAmap*win_azim, n=abins_num, axis=2)

    RAmap = fftshift(RAmap, 2)
    
    RAmap = RAmap.real**2 + RAmap.imag**2

    ramap_sum = np.sum(RAmap[:, :, :], axis=0)

    # часть для ослабления амплитуд сигналов в ближней зоне:
    # ramap_sum_near_atten = np.copy(ramap_sum)
    # range_of_range = np.shape(ramap_sum_near_atten)[0]
    # for i in range(np.shape(ramap_sum_near_atten)[0]):
    #     ramap_sum_near_atten[i, :] = \
    #         ramap_sum_near_atten[i, :]/(range_of_range-i)**2

    # return ramap_sum_near_atten, RAmap
    return ramap_sum, RAmap

def partRaMapBuilding(input_cube, prms):
    """
    расчёт двумерной карты "дальность-азимут" для скоростей,
    где присутствуют цели.

    Для каждого импульса выполняются:
        1. преобразование Фурье вдоль оси отсчётов внутри импульса;
        2. преобразование Фурье вдоль оси виртуальных приёмников.

    Полученные после преобразований Фурье двумерные карты
    в количестве импульсов складываются, т.о. строится
    суммарная карта "дальность-азимут".

    Входные параметры:
        1. input_cube - входной куб сырых радарных данных
        2. prms - массив размеров входной и возвращаемой карт,
           prms['smp_num'] - количество отсчётов в импульсах
           prms['rxq_num'] - количество виртуальных приёмников
           prms['rbins_num'] - количество отсчётов по дальности
           prms['abins_num'] - количество отсчётов по азимуту

    Возвращаемые параметры:
        ramap_sum - суммарная RV-карта
        RAcube - куб данных после выполнения двумерного преобразования \
                 Фурье на каждом виртуальном приёмнике
    """

    c = 299792458.0
    f = 77e9
    lmb = c/f
    d = lmb/2

    f_ang = np.fft.fftfreq(2048, d)
    angles = np.rad2deg(np.arcsin(f_ang * lmb))
    angles = np.fft.fftshift(angles[:]*1)

    smp_num = prms['smp_num']
    rxq_num = prms['rxq_num']
    rbins_num = prms['rbins_num']
    abins_num = prms['abins_num']
    rbeta = prms['range_beta']
    abeta = prms['azims_beta']

    # Выполнение FFT по дальности для MIMO субкадра
    win_range = signal.hamming(smp_num)
    win_azim = signal.hamming(rxq_num)
    plots = False

    if (plots):
        ########################
        plt.figure()
        plt.title("Окно Хэмминга, (используется для дальностей)")
        plt.plot(win_range)

        ########################
        plt.figure()
        plt.title("АЧХ окна Хэмминга, (используется для дальностей)")
        plt.plot(angles, 10*np.log10(fftshift(abs(fft(win_range, 2048)))))
        plt.xlim([-85, 85])
        plt.ylim([-70, 21.5])
        plt.xlabel('angle [degrees]')
        plt.ylabel('signal')
        plt.xlabel('angle [rad]')

        ########################
        plt.figure()
        plt.title("Окно Хэмминга, (используется для азимутов)")
        plt.plot(win_azim)
        ########################

        plt.figure()
        plt.title("АЧХ окна Хэмминга, (используется для азимутов)")
        plot_mas = 10*np.log10(fftshift(abs(fft(win_azim, 2048))))
        plt.plot(angles, plot_mas)
        # plt.axhline(y=0.707*np.max(plot_mas), color='b', linestyle='--',
        #             lw = 1)
        plt.xlim([-85, 85])
        plt.ylim([-70, 16])
        plt.xlabel('angle [degrees]')
        plt.ylabel('signal')
        plt.xlabel('angle [rad]')

    ######################################################################
#    win_range = windows.kaiser(smp_num, beta=rbeta)
#    win_azim = windows.kaiser(rxq_num, beta=abeta)

    if (plots):
        plt.figure()
        plt.plot(win_range)
        plt.title("Окно Кайзера при Б, равном 10 (используем для дальностей)")

        #########################
        plt.figure()
        plt.title("АЧХ окна Кайзера при Б, равном 10 (256 входных отсчёта)")
        plt.plot(angles, 10*np.log10(fftshift(abs(fft(win_range, 2048)))))
        plt.xlim([-85, 85])
        plt.ylim([-70, 21.5])
        plt.xlabel('angle [degrees]')
        plt.ylabel('signal')
        plt.xlabel('angle [rad]')

        #########################
        plt.figure()
        plt.plot(win_azim)
        plt.title("Окно Кайзера при Б, равном 20 (используется для азимутов)")

        #########################
        plt.figure()
        plot_mas = 10*np.log10(fftshift(abs(fft(windows.kaiser(rxq_num, 15),
                                                2048))))
        plt.plot(angles, plot_mas)
        # plt.axhline(y=0.707*np.max(plot_mas), color='r', linestyle='--',
        #             lw = 1)
        plt.title("АЧХ окна Кайзера при Бета, равном 20 (64 входных отсчёта)")
        plt.xlim([-85, 85])
        plt.ylim([-70, 16])
        plt.xlabel('angle [degrees]')
        plt.ylabel('signal')
        plt.xlabel('angle [rad]')

#    input_cube_ = input_cube[prms['non_zero_velocs'], :, :]
    # RAmap = fft(input_cube_*win_range[None, :, None],
    #             n=rbins_num, axis=1)

    # Выполнение FFT по азимуту для MIMO субкадра

#    RAmap = fft(input_cube_*win_azim, n=abins_num, axis=2)
    RAmap = fft(input_cube*win_azim, n=abins_num, axis=2)

    RAmap = fftshift(RAmap, 2)
    RAmap = RAmap.real**2 + RAmap.imag**2

    ramap_sum = np.sum(RAmap[:, :, :], axis=0)

    # часть для ослабления амплитуд сигналов в ближней зоне:
    # ramap_sum_near_atten = np.copy(ramap_sum)
    # range_of_range = np.shape(ramap_sum_near_atten)[0]
    # for i in range(np.shape(ramap_sum_near_atten)[0]):
    #     ramap_sum_near_atten[i, :] = \
    #         ramap_sum_near_atten[i, :]/(range_of_range-i)**2

    # return ramap_sum_near_atten, RAmap
    return ramap_sum, RAmap

def oneDimCFAR(input_data, prms, vertHorizFlag):
    """
    Нахождение пиков с помощью алгоритма CFAR на одномерных массивах.

    Вспомогательная функция для последующих расчётов двумерных карт

    Входные параметры:
        1. input_data - входной одномерный массив
        2. prms - массив параметров, необходимых для \
           выполнения алгоритма CFAR,
           prms['num_train'] -  размеры окна анализа \
           (после вычета защитного интервала)
           prms['num_guard'] - размер окна защитного интервала
           prms['Dfa_rate'] - вероятность ложного обнаружения\
                              по доплеру (Dfa_rate)
           prms['Rfa_rate'] - вероятность ложного обнаружения\
                              по дальности (Rfa_rate)
        3. vertHorizFlag - флаг на выполнение алгоритма в вертикальной \
           или горизонтальной плоскости,
           0 - в вертикальной
           1 - в горизонтальной
           флаг необходим для выбора требуемой вероятности ложной тревоги

    Возвращаемый параметр:
        peak_idx - массив индексов, на которых обнаружены пики
    """

    num_cells = input_data.size
    num_train = prms['num_train'][0]
    num_guard = prms['num_guard'][0]
    num_train_half = round(num_train / 2)
    num_guard_half = round(num_guard / 2)
    num_side = num_train_half + num_guard_half
    if vertHorizFlag == 0:
        fa_rate = prms['Rfa_rate']
    if vertHorizFlag == 1:
        fa_rate = prms['Dfa_rate']

    # threshold factor
    alpha = num_train_half*(fa_rate**(-1/num_train_half) - 1)

    peak_idx = []
    noise = np.array([])
    for i in range(1, num_cells - 1):

        left_idx = max(0, i - num_side)
        right_idx = min(num_cells - 1, i + num_side)
#        if i != left_idx + np.argmax(x[left_idx:right_idx + 1]):
#            continue
        if (input_data[i - 1] < input_data[i] >= input_data[i + 1]):
            if (i < num_side):
                sum1 = np.sum(input_data[i:right_idx + 1])
                sum2 = np.sum(input_data[i:i + num_guard_half + 1])
                Z = (sum1 - sum2)/num_train_half
            elif (i >= num_cells - num_side):
                sum1 = np.sum(input_data[left_idx:i + 1])
                sum2 = np.sum(input_data[i - num_guard_half:i + 1])
                Z = 2*(sum1 - sum2)/num_train_half
            else:
                Z_left = np.sum(input_data[left_idx:i + 1]) \
                         - np.sum(input_data[i - num_guard_half:i + 1])
                Z_right = np.sum(input_data[i:right_idx + 1]) \
                    - np.sum(input_data[i:i + num_guard_half + 1])
                Z = min(Z_left, Z_right)/num_train_half

            threshold = alpha*Z

            if input_data[i] > threshold:
                peak_idx.append(i)
                noise = np.append(noise, Z)

    peak_idx = np.array(peak_idx, dtype=int)

    return peak_idx


def vhCFAR(input_data, prms):
    """
    Нахождение пиков с помощью алгоритма CFAR на двумерных картах.

    Алгоритм выполняется с помощью вычисления одномерных функций CFAR
    в вертикальной и горизонтальной плоскостях исходной карты.

    Входные параметры:
        1. input_data - входной двумерный массив
        2. prms - массив параметров, необходимых для \
           выполнения алгоритма CFAR,
           prms['num_train'] -  размеры окна анализа \
           (после вычета защитного интервала)
           prms['num_guard'] - размер окна защитного интервала
           prms['Dfa_rate'] - вероятность ложного обнаружения\
                              по доплеру (Dfa_rate)
           prms['Rfa_rate'] - вероятность ложного обнаружения\
                              по дальности (Rfa_rate)
    Возвращаемый параметр:
        rdTrgts - двумерный массив отсчётов скоростей и дальностей, \
                  на которых обнаружены пики
    """

    w = np.shape(input_data)[0]
    h = np.shape(input_data)[1]

    rvacc = input_data**1

    rCFARres = np.zeros(h, float)
    # флаг, указывающий, что в обработанной строке найдены цели;
    # обработка по второй оси производится только лишь в строках
    # с ненулевым флагом
    nonZeroStrings = np.zeros(h, float)
    rvCFARres = np.zeros([w, h], float)

    peak_idx = []

    for i in range(w):
        rCFARres = oneDimCFAR(rvacc[i, :], prms, 0)
        rvCFARres[i, rCFARres] = input_data[i, rCFARres]
        nonZeroStrings[rCFARres] = 1

    vCFARres = np.zeros(w, float)
    for j in range(h):
        if (nonZeroStrings[j] != 0):
            vCFARres = oneDimCFAR(rvCFARres[:, j], prms, 1)
            for i in range(np.shape(vCFARres)[0]):
                peak_idx.append([vCFARres[i], j])

    peak_idx = np.array(peak_idx, dtype=int)

    if peak_idx.any():
        pntsRange = peak_idx[:, 1]
        pntsVel = peak_idx[:, 0]
    else:
        print("peak_idx is empty")
        pntsRange = 0
        pntsVel = 0

    rdTrgts = (pntsVel, pntsRange)

    return rdTrgts


def twoDimsCFAR(input_data, prms):
    """
    Нахождение пиков с помощью алгоритма CFAR на двумерных картах.

    Выполняется с помощью двумерного алгоритма адаптивного определения \
    порога на основе усреднения окна 2D-CFAR (constant false alarm rate).

    Входные параметры:
        1. input_data - входной двумерный массив
        2. prms - массив параметров, необходимых для \
           выполнения алгоритма CFAR,
           prms['num_train'] -  размеры окна анализа \
           (после вычета защитного интервала)
           prms['num_guard'] - размер окна защитного интервала
           prms['fa_rate'] - вероятность ложного обнаружения
    Возвращаемый параметр:
        rdTrgts - двумерный массив отсчётов скоростей и дальностей,\
                  на которых обнаружены пики

    """
    num_train = prms['num_train']
    num_guard = prms['num_guard']
    fa_rate = prms['fa_rate']
    num_train_half = np.array(np.round(np.array(num_train, dtype=int) / 2),
                              dtype=int)
    num_guard_half = np.array(np.round(np.array(num_guard, dtype=int) / 2),
                              dtype=int)
    num_side = num_train + num_guard
    # расширяем массив для обнаружения на краях
    rvacc = input_data**2
    num_cells = rvacc.shape
    rvacc = np.concatenate((rvacc[:, num_cells[1] - num_side[1]::],
                            rvacc,
                            rvacc[:, 0:num_side[1]]), axis=1)
    rvacc = np.concatenate((rvacc[num_cells[0] - num_side[0]::, :],
                            rvacc,
                            rvacc[0:num_side[0], :]), axis=0)

    # threshold factor
    trainfactor = np.product(num_train)
    alpha = trainfactor*(fa_rate**(-1/trainfactor) - 1)
    peak_idx = []
    noise = np.array([])

    for i in range(num_side[0], num_cells[0] - num_side[0]):
        for j in range(num_side[1], num_cells[1] - num_side[1]):
            # aperture = rvacc[i - 1:i + 2, j - 1:j + 2]
            # maxarg = np.unravel_index(np.argmax(aperture),
            #                           aperture.shape)
            # if (maxarg != (1, 1)):
            #     continue

            sum1 = np.sum(rvacc[i - num_side[0]:i + num_side[0] + 1,
                                j - num_side[1]:j + num_side[1] + 1])

            sum2 = np.sum(rvacc[i - num_guard_half[0]:
                                i + num_guard_half[0] + 1,
                                j - num_guard_half[1]:
                                j + num_guard_half[1] + 1])

            p_noise = (sum1 - sum2) / trainfactor
            threshold = alpha * p_noise
            if rvacc[i, j] > threshold:
                peak_idx.append(np.array([i, j]))
                noise = np.append(noise, p_noise)

    peak_idx = np.array(peak_idx, dtype=int)

    if peak_idx.any():
        pntsRange = peak_idx[:, 1] - num_side[1]
        pntsVel = peak_idx[:, 0] - num_side[0]
    else:
        print("peak_idx is empty")
        pntsRange = 0
        pntsVel = 0

    rdTrgts = (pntsVel, pntsRange)

    return rdTrgts

def pref_sums_cfar(input_data, prms):
    
    # input_data = np.fft.fftshift(input_data, 0)
    num_train = prms['num_train']
    num_guard = prms['num_guard']
    fa_rate = prms['fa_rate']
    num_train_half = np.array(np.round(np.array(num_train, dtype=int) / 2),
                              dtype=int)
    num_guard_half = np.array(np.round(np.array(num_guard, dtype=int) / 2),
                              dtype=int)
    num_side = []
    num_side.append(num_train[0] + num_guard[0])
    num_side.append(num_train[1] + num_guard[1])
    # расширяем массив для обнаружения на краях
    rvacc = input_data**2
    num_cells = rvacc.shape

    rvacc = np.concatenate((rvacc[0:num_side[0], :],
                            rvacc,
                            rvacc[num_cells[0] - num_side[0]::, :]), axis=0)
    rvacc = np.concatenate((rvacc[:, 0:num_side[1]],
                            rvacc,
                            rvacc[:, num_cells[1] - num_side[1]::]), axis=1)
    # threshold factor
    trainfactor = np.product(num_train)
    alpha = trainfactor*(fa_rate**(-1/trainfactor) - 1)
    peak_idx = []
    noise = np.array([])

    n = num_cells[0]
    m = num_cells[1]
    res_pref_sum_2d = [[0 for j in range(m+1)] for i in range(n+1)]

    for i in range(n):
        for j in range(m):
            res_pref_sum_2d[i + 1][j + 1] = res_pref_sum_2d[i][j + 1] + \
                res_pref_sum_2d[i + 1][j] - res_pref_sum_2d[i][j] + \
                rvacc[i][j]
                
    full_win_side = 2 * (num_train[0] + num_guard[0]) + 1
    guard_win_side = 2 * (num_guard[0] + 1)
    analize_square = full_win_side * full_win_side - \
        guard_win_side * guard_win_side

    for i in range(num_side[0], num_cells[0] - num_side[0]):
        for j in range(num_side[1], num_cells[1] - num_side[1]):
            aperture = rvacc[i - 1:i + 2, j - 1:j + 2]
            maxarg = np.unravel_index(np.argmax(aperture),
                                      aperture.shape)
            if (maxarg != (1, 1)):
                continue
        
            # big sum
            lx = i - num_side[0]
            ly = j - num_side[1]
            rx = i + num_side[0] + 1
            ry = j + num_side[1] + 1
            big_sum = res_pref_sum_2d[rx][ry] - res_pref_sum_2d[lx][ry] - \
                res_pref_sum_2d[rx][ly] + res_pref_sum_2d[lx][ly]
            # small sum
            lx = i - num_guard[0]
            ly = j - num_guard[1]
            rx = i + num_guard[0] + 1
            ry = j + num_guard[1] + 1
            small_sum = res_pref_sum_2d[rx][ry] - res_pref_sum_2d[lx][ry] - \
                res_pref_sum_2d[rx][ly] + res_pref_sum_2d[lx][ly]
            p_noise = (big_sum - small_sum) / trainfactor
            threshold = alpha * p_noise
            if rvacc[i, j] > threshold:
                peak_idx.append(np.array([i, j]))
                noise = np.append(noise, p_noise)

    peak_idx = np.array(peak_idx, dtype=int)

    if peak_idx.any():
        pntsRange = peak_idx[:, 1] - num_side[1]
        pntsVel = peak_idx[:, 0] - num_side[0]
    else:
        print("peak_idx is empty")
        pntsRange = 0
        pntsVel = 0

    rdTrgts = (pntsVel, pntsRange)
    return rdTrgts


def detect_peaks(input_data, num_train, num_guard, rate_fa):

    num_cells = input_data.size
    num_train_half = round(num_train / 2)
    num_guard_half = round(num_guard / 2)
    num_side = num_train_half + num_guard_half

    alpha = num_train*(rate_fa**(-1/num_train) - 1)  # threshold factor

    peak_idx = []
    for i in range(num_side, num_cells - num_side):

        if i != i - num_side+np.argmax(input_data[i -
                                                  num_side:i + num_side + 1]):
            continue

        sum1 = np.sum(input_data[i-num_side:i+num_side+1])
        sum2 = np.sum(input_data[i-num_guard_half:i+num_guard_half+1])
        p_noise = (sum1 - sum2) / num_train
        threshold = alpha * p_noise

        if input_data[i] > threshold:
            peak_idx.append(i)

    peak_idx = np.array(peak_idx, dtype=int)

    return peak_idx


def partialCFAR(input_data, prms, targs):
    """
    Нахождение пиков с помощью алгоритма CFAR на двумерных картах \
    в окрестностях заданных пар дальность-скорость.
        
    Заданные RV-пары находятся путём выполнением полного CFAR на другом \
    субкадре в пределах одного кадра. 
        
    Функция уменьшает количество выпоняемых операций в \
    от rbins_num*dbins_num/targsNum \
    до rbins_num*dbins_num/(targsNum*num_train) раз в сравнении с обычным CFAR

    Выполняется с помощью двумерного алгоритма адаптивного определения \
    порога на основе усреднения окна 2D-CFAR (constant false alarm rate).

    Входные параметры:
        1. input_data - входной двумерный массив
        2. prms - массив параметров, необходимых для \
           выполнения алгоритма CFAR,
           prms['num_train'] -  размеры окна анализа \
           (после вычета защитного интервала)
           prms['num_guard'] - размер окна защитного интервала
           prms['fa_rate'] - вероятность ложного обнаружения
        3. набор бинов дальность-скорость, в окрестностях которых \
           производится рассчёт
    Возвращаемый параметр:
        rdTrgts - двумерный массив отсчётов скоростей и дальностей,\
                  на которых обнаружены пики

    """
    # velocs = targs['velocities']
    # ranges = targs['ranges']
    num_train = prms['num_train']
    num_guard = prms['num_guard']
    fa_rate = prms['fa_rate']
    num_train_half = np.array(np.round(np.array(num_train, dtype=int) / 2),
                              dtype=int)
    num_guard_half = np.array(np.round(np.array(num_guard, dtype=int) / 2),
                              dtype=int)
    num_side = num_train_half + num_guard_half
    # расширяем массив для обнаружения на краях
    rvacc = input_data**2
    num_cells = rvacc.shape
    rvacc = np.concatenate((rvacc[:, num_cells[1] - num_side[1]::],
                            rvacc,
                            rvacc[:, 0:num_side[1]]), axis=1)
    rvacc = np.concatenate((rvacc[num_cells[0] - num_side[0]::, :],
                            rvacc,
                            rvacc[0:num_side[0], :]), axis=0)

    # threshold factor
    trainfactor = np.product(num_train)
    alpha = trainfactor*(fa_rate**(-1/trainfactor) - 1)
    peak_idx = []
    noise = np.array([])

    for k in range(len(targs)):
        i = targs[k, 1] + num_side[0]
        j = targs[k, 0] + num_side[1]

        # aperture = rvacc[i - 1:i + 2, j - 1:j + 2]
        # maxarg = np.unravel_index(np.argmax(aperture),
        #                           aperture.shape)
        # if (maxarg != (1, 1)):
        #     continue

        sum1 = np.sum(rvacc[i - num_side[0]:i + num_side[0] + 1,
                            j - num_side[1]:j + num_side[1] + 1])

        sum2 = np.sum(rvacc[i - num_guard_half[0]:
                            i + num_guard_half[0] + 1,
                            j - num_guard_half[1]:
                            j + num_guard_half[1] + 1])

        p_noise = (sum1 - sum2) / trainfactor
        threshold = alpha * p_noise
        m = 0
        n = 0
        breakFlag = 0
        if rvacc[i, j] > threshold:
            peak_idx.append(np.array([i+m, j+n]))
            noise = np.append(noise, p_noise)
        else:
            for m in range(-num_side[0]+1, num_side[0]):
                for n in range(-num_side[1]+1, num_side[1]):
                    if rvacc[i+m, j+n] > threshold:
                        peak_idx.append(np.array([i+m, j+n]))
                        noise = np.append(noise, p_noise)
                        breakFlag = 1
                        break 
                if breakFlag == 1:
                    breakFlag = 0
                    break

    peak_idx = np.array(peak_idx, dtype=int)

    if peak_idx.any():
        pntsRange = peak_idx[:, 1] - num_side[1]
        pntsVel = peak_idx[:, 0] - num_side[0]
    else:
        print("peak_idx is empty")
        pntsRange = 0
        pntsVel = 0

    rdTrgts = (pntsVel, pntsRange)

    return rdTrgts

def dopplerCorrection(input_cube, rdTrgts, radarParams, rVel):
    """
    Коррекция отсчётов по азимуту в соответствии с найденными оценками
    скоростей целей.

    Для каждой найденной пары дальность-скорость в кубе данных \
    извлекаются отсчёты со всех виртуальных приёмников. Фазы этих \
    отсчётов имеют сдвиг из-за доплеровского набега частоты, который \
    необходимо устранить.

    Входные параметры:
        1. input_cube - куб данных, после выполнения функции \
                        rvMapBuilding (часть куба, для которой требуется \
                        компенсация фаз)
        2. rdTrgts - двумерный массив отсчётов скоростей, дальностей и \
                     скоростей в км/ч, на которых обнаружены пики
        3. radarParams - словарь с параметрами принимаемого сигнала
        4. rVel - дополнительная постоянная составляющая скорости (опционально)

    Возвращаемый параметр:
        correctAzims - массив отсчётов сигнала на виртуальных приёмниках \
                       после выполнения корректировки фаз, готовый для \
                       нахождения азимутов объектов
    """
    tx_num = 3
    pntsVel = rdTrgts[0]
    pntsRng = rdTrgts[1]
    pntsRlVel = rdTrgts[2]
    virtRxNum = np.shape(input_cube)[2]
    tp = 3 * (radarParams['profileRampEndTime'] +
              radarParams['profileIdleTimeConst'])
    c = 299792458.0
    lmb = c / radarParams['profileStartFreqConst']
    # radarParams.profileFreqSlopeConst
    # pntsRlVel[:] = -rVel
    mult_coef = np.exp(-1j * ((np.pi * pntsRlVel * (2*tp)/tx_num) / lmb))

    correctAzims = np.ones([len(pntsVel), virtRxNum], complex)
    for i in range(4, virtRxNum):
        correctAzims[:, i] = mult_coef[:]

    rvmapForCorr = input_cube[pntsVel, pntsRng, :]
    correctAzims = rvmapForCorr*correctAzims
    return correctAzims


idx = 0


def angls_dtct_median(angels, rdTrgts, prms, max_lvl, edge):
    """
    Функция поиска максимумов на профиле азимутов и нахождение угла места для
    данного азимута.
    max_lvl - пороговое значение, т.е.
    (во сколько раз максимум должен превосходить среднее значение)
    edge - число неучитываемых отсчётов (по краям) 2*edge < self.abins_num
    """
    elev = 'off'
    azimuths = angels['azimuths']
    elevations = angels['elevations']

    hamwin_azm = signal.hamming(prms['rxq_num_az'])
    hamwin_elev = signal.hamming(4)
    abins_num = prms['abins_num']
    elevBins_num = 64

    pntsVel = rdTrgts[2]
    pntsRng = rdTrgts[1]
    pntsNum = len(pntsRng)

    pntsAzmFFT = np.array([], dtype=int)
    pntsElevFFT = np.array([], dtype=int)
    pntsRngFFT = np.array([], dtype=int)
    pntsVelFFT = np.array([], dtype=int)
    pntsMagAz = np.array([], dtype=float)

    azimuths = azimuths * hamwin_azm

    azimProfileFFT = fft(azimuths, n=abins_num)
    # azimProfileFFT = (azimProfileFFT.real**2 + azimProfileFFT.imag**2)
    azimProfileFFT = np.fft.fftshift(abs(azimProfileFFT), 1)

    azimProfileAcc = np.array([np.sum(enum[edge:abins_num-edge])
                               for enum in azimProfileFFT])
    profile_max_lvl = azimProfileAcc*max_lvl/float(abins_num - 2*edge)

    prof_d = [(azProf[1:].copy() - azProf[:len(azProf)-1])
              for azProf in azimProfileFFT]
    prof_d = [(np.append(prof_d_iter[0], prof_d_iter))
              for prof_d_iter in prof_d]

    if (elev == 'on'):
        elevations = np.array(elevations*hamwin_elev)

        elevationsPad = [np.concatenate(([0, 0], elev, [0, 0]))
                         for elev in elevations]
        elevationsPad = np.array(elevationsPad)
        azEl = np.array([azimuths.T, elevationsPad.T]).T

        max_az_idx = np.array([np.argmax(azimProfileFFT[i, :])
                               for i in range(pntsNum)])

        azElMap = np.array([np.fft.fftn(elem, s=(abins_num, elevBins_num),
                                        axes=(0, 1)) for elem in azEl])
        azElMap = np.fft.fftshift(azElMap, axes=(1, 2))

    # максимумы профиля
    for i in range(pntsNum):
        for ref in range(edge, abins_num-edge):
            if ((prof_d[i][ref] >= 0) and (prof_d[i][ref+1] <= 0)):
                if (azimProfileFFT[i][ref] > profile_max_lvl[i]):
                    newMag = azimProfileFFT[i][ref]
                    if (elev == 'on'):
                        targAzElMap = abs(azElMap[i])
                        elevProf = targAzElMap[ref, :]
                        trgtElev = np.argmax(elevProf)
                        if ((trgtElev >= 0) and (trgtElev <= elevBins_num)):
                            pntsMagAz = np.append(pntsMagAz, newMag)
                            pntsAzmFFT = np.append(pntsAzmFFT, ref)
                            pntsElevFFT = np.append(pntsElevFFT, trgtElev)
                            pntsRngFFT = np.append(pntsRngFFT, pntsRng[i])
                            pntsVelFFT = np.append(pntsVelFFT, pntsVel[i])
                    else:
                        pntsMagAz = np.append(pntsMagAz, newMag)
                        pntsAzmFFT = np.append(pntsAzmFFT, ref)
                        pntsElevFFT = np.append(pntsElevFFT, elevBins_num/2)
                        pntsRngFFT = np.append(pntsRngFFT, pntsRng[i])
                        pntsVelFFT = np.append(pntsVelFFT, pntsVel[i])

    # plt.figure()
    # plt.plot(azimProfileFFT[idx])
    # plt.figure()
    # plt.plot(abs(azElMap[idx, np.argmax(azimProfileFFT[idx])]))
    # plt.figure()
    # # buf1 = np.copy(azElMap[idx][:, 4:])
    # # buf2 = np.copy(azElMap[idx][:, :4])
    # # azElMap[idx][:, :60] = buf1
    # # azElMap[idx][:, 60:] = buf2
    # azElMap[idx][np.argmax(azimProfileFFT[idx]),
    #              np.argmax(abs(azElMap[idx,
    #                                    np.argmax(azimProfileFFT[idx])]))] = 0
    # azElMap[idx][np.argmax(azimProfileFFT[idx]) + 1,
    #              np.argmax(abs(azElMap[idx,
    #                                    np.argmax(azimProfileFFT[idx])]))] = 0
    # azElMap[idx][np.argmax(azimProfileFFT[idx]) - 1,
    #              np.argmax(abs(azElMap[idx,
    #                                    np.argmax(azimProfileFFT[idx])]))] = 0
    # azElMap[idx][np.argmax(azimProfileFFT[idx]),
    #              np.argmax(abs(azElMap[idx,
    #                                    np.argmax(azimProfileFFT[idx])])) + 1] = 0
    # azElMap[idx][np.argmax(azimProfileFFT[idx]),
    #              np.argmax(abs(azElMap[idx,
    #                                    np.argmax(azimProfileFFT[idx])])) - 1] = 0
    # plt.imshow(abs(azElMap[idx].T))

    resMas = {'azimuths': pntsAzmFFT,
              'elevations': pntsElevFFT,
              'ranges': pntsRngFFT,
              'velocities': pntsVelFFT,
              'magnitude': pntsMagAz}

    return resMas


def XYZVbuild(angels, rdTrgts, radarParams, prms):
    """
    Для каждой найденной пары дальность-скорость в кубе данных \
    извлекаются отсчёты со всех виртуальных приёмников. Выполняется \
    преобразование Фурье от этих отсчётов, извлекается максимальное \
    значение - азимут цели на заданных дальности и скорости.

    Производится пересчёт из полярных координат в декартовы, т.о. \
    формируется облако точек целей, обнаруженных радаром. Полученное \
    облако - массив, содержащий координаты точек X, Y и \
    радиальную скорость, а также Z для целей в радиусе 30 метров.

     Входные параметры:
        1. angels - массив данных, содержащий отсчёты по виртуальным \
                        приёмникам для каждой цели
        2. rdTrgts - массив отсчётов скоростей, дальностей и реальных \
                     скоростей, на которых обнаружены пики
        3. radarParams - словарь с параметрами принимаемого сигнала
        4. prms - словарь размеров входной и возвращаемой карт,
           prms['smp_num'] - количество отсчётов в импульсах
           prms['pls_num'] - количество импульсов
           prms['rxq_num_az'] - количество виртуальных приёмников по азим
           prms['rbins_num'] - количество отсчётов по дальности
           prms['abins_num'] - количество отсчётов по азимуту
           prms['dbins_num'] - количество отсчётов по доплеру

    Возвращаемый параметр:
        XYVcoord - трёхмерный массив координат найденных целей
    """

    pntsVel = rdTrgts[2]
    pntsRng = rdTrgts[1]

    rbins_num = prms['rbins_num']
    abins_num = prms['abins_num']
    # sfidx = prms['sfidx']

    fs = radarParams['profileDigOutSampleRate']
    tp = 3 * (radarParams['profileRampEndTime'] +
              radarParams['profileIdleTimeConst'])
    c = 299792458.0
    lmb = c / radarParams['profileStartFreqConst']
    S = radarParams['profileFreqSlopeConst']

    resMas = angls_dtct_median(angels, rdTrgts, prms, max_lvl=1.9, edge=1)
    pntsVel = resMas['velocities']
    ebins_num = 64

    freq_range = np.array([])
    # for i in range(pntsNum):
    freq_range = ((resMas['ranges'] * fs) / rbins_num)
    freq_azimuth = ((resMas['azimuths'] - int(abins_num/2)) /
                    (abins_num * lmb/2))
    freq_elevats = ((resMas['elevations'] - int(ebins_num/2)) /
                    (ebins_num * lmb/2))

    X_coord = ((freq_range * tp * c)/(2 * S * tp)) * \
        np.sin(np.arcsin(freq_azimuth*lmb))*np.cos(np.arcsin(freq_elevats*lmb))
    Z_coord = -((freq_range * tp * c)/(2 * S * tp)) * \
        np.sin(np.arcsin(freq_elevats*lmb))
    Y_coord = np.sqrt(((freq_range * tp * c)/(2 * S * tp))**2 - X_coord**2
                      - Z_coord**2)

    print(Y_coord[idx], X_coord[idx], Z_coord[idx])
    print("idx range is ", resMas['ranges'][idx])

    XYVcoord = (X_coord, Y_coord, pntsVel, resMas['azimuths'])
    return XYVcoord


def getFrameData(prms, radarType, routeBinFile, frameNum=0):
    """
    Чтение отсчётов с выхода АЦП радара и представление в форме
    куба данных, где вдоль граней расположены отсчёты, соответствующие
    импульсам, отсчётам внутри импульсов и виртуальным приёмникам.

    Входные параметры:
        1. prms - массив размеров входной и возвращаемой карт,
           prms['smp_num'] - количество отсчётов в импульсах
           prms['pls_num'] - количество импульсов
           prms['rxq_num'] - количество виртуальных приёмников
           prms['rbins_num'] - количество отсчётов по дальности
           prms['abins_num'] - количество отсчётов по азимуту
           prms['dbins_num'] - количество отсчётов по доплеру
        2. radarType - тип используемого радара
                       'mrl' - при работе с макетом
                       'dca' - при работе с отладкой
        3. routeBinFile - путь к файлу с данными с выхода АЦП
        4. frameNum - номер считываемого кадра, по умолчанию равен 0

    Возвращает:
        datacube - заполненный куб исходных данных в формате: \
                   [pulses, samples, virtual_receivers]
    """

    smp_num = prms['smp_num']
    pls_num = prms['pls_num']
    rxq_num = prms['rxq_num']
    rx_num = prms['rx_num']
    tx_num = prms['tx_num']
    
    count = smp_num*pls_num*rx_num*tx_num*2 

    data = open(routeBinFile, 'rb')
    position = count*frameNum
    data.seek(position, 0)

    if (radarType == 'dca'):
        Xt = np.fromfile(data, dtype=np.dtype('int16'), count=count)
    if (radarType == 'mrl'):
        Xt = np.fromfile(data, dtype=np.dtype('int16'), count=count)
    # Выделение каждого второго элемента начиная с 0. I - составляющая
    Xt_I = Xt[::2]
    # Выделение каждого второго элемента начиная с 1. Q - составляющая
    Xt_Q = Xt[1::2]

    if (radarType == 'mrl'):
        # Xt_I = np.transpose(np.reshape(Xt_I, [pls_num, tx_num, rx_num,
        #                                       smp_num]), (0, 2, 1))
        # Xt_Q = np.transpose(np.reshape(Xt_Q, [pls_num, tx_num, rx_num,
        #                                       smp_num]), (0, 2, 1))
        Xt_I = np.reshape(Xt_I, [pls_num, tx_num, rx_num, smp_num])
        Xt_Q = np.reshape(Xt_Q, [pls_num, tx_num, rx_num, smp_num])

    if (radarType == 'dca'):
        Xt_I = np.transpose(np.reshape(Xt_I, [pls_num, tx_num, smp_num,
                                              rx_num]), (0, 2, 1, 3))
        if (prms['tx_num'] == 1):
            Xt_I = Xt_I[:, :, 0, :]
        else:
            Xt_I = np.concatenate(Xt_I, axis=2)
            
        Xt_Q = np.transpose(np.reshape(Xt_Q, [pls_num, tx_num, smp_num,
                                              rx_num]), (0, 2, 1, 3))
        if (prms['tx_num'] == 1):
            Xt_Q = Xt_Q[:, :, 0, :]
        else:
            Xt_Q = np.concatenate(Xt_Q, axis=2)

    dataCube = Xt_I + 1j*Xt_Q
    return dataCube


def find_nearest(array, value):
    """
    Функция **find_nearest(array, value)**
    
    Нахождение ближайшего значения в массиве
    
    Входные параметры:
        1. array - входной массив
        2. value - значение, ближайшее к которому необходимо найти
    
    Возвращает:
        1.idx - индекс ближайшего значения
    """
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx


def eulerAnglesToRotationMatrix(translate, theta):
    """
    Функция **eulerAnglesToRotationMatrix(translate,theta)**
        
    Расчёт матрицы трансляции поворота на основании углов Эйлера и смещение по осям
    
    Входные параметры:
        1. translate - входной массив 1*3 смещения по осям
        2. theta - входной массив 1*3 углов Эйлера
    
    Возвращает:
        1. R - матрицу трансляции поворота
    """
    
    R_x = np.array([[1, 0, 0],
                    [0, np.cos(theta[0]), -np.sin(theta[0])],
                    [0, np.sin(theta[0]), np.cos(theta[0])]
                    ])

    R_y = np.array([[np.cos(theta[1]), 0, np.sin(theta[1])],
                    [0, 1, 0],
                    [-np.sin(theta[1]), 0, np.cos(theta[1])]
                    ])

    R_z = np.array([[np.cos(theta[2]), -np.sin(theta[2]), 0],
                    [np.sin(theta[2]), np.cos(theta[2]), 0],
                    [0, 0, 1]
                    ])

    R = np.dot(R_z, np.dot(R_y, R_x))
    T = np.array([  [translate[0]],
                    [translate[1]],
                    [translate[2]]
                    ])
    One = np.array([[0],
                    [0],
                    [0],
                    [1]
                    ])
    RT = np.concatenate((R,T), axis=1)
    RT = np.concatenate((RT, One.T))

    return RT

def threshEstimationMedian(prof, blockNum, blockSize, noiseScale):
    block_lvl = []
    for block_ref in range(0, blockNum):
        lvl_temp = np.sum(prof[block_ref*blockSize : (block_ref+1)*blockSize])
        lvl_temp /= blockSize
        block_lvl.append(lvl_temp)
    block_lvl.sort()
    
    return block_lvl[blockNum>>2]*noiseScale

def localMaximaDetector(rvacc, params):
    smp_pad = rvacc.shape[1]
    pls_pad = rvacc.shape[0]
    # параметры для оценки уровня шума
    blockNumR  = 1 << params["blockR"]   # число блоков на которых оценивается уровень шума
    blockSizeR = smp_pad >> params["blockR"]  # размер блока
    blockNumV  = 1 << params["blockV"]
    blockSizeV = pls_pad >> params["blockV"]
    
    pnts = []  # список для записи координат локальных максимумов

    # маска с дальностями, на которых расчитаны пороги по скорости
    v_mask   = np.zeros(smp_pad, int)
    # массив с порогами для каждого профиля скорости
    v_thresh = np.zeros(smp_pad, float)
    # массив с порогами для каждого профиля дальности
    r_thresh = np.zeros(pls_pad, float)
    
    # детектирование на основе RV-карты
    for vel_ref in range(1, pls_pad-1):
        # анализируемый профиль дальности (столбец RV-карты)
        r_prof = rvacc[vel_ref, :]
        
        # оценка уровня шума на профиле дальности
        r_thresh[vel_ref] = threshEstimationMedian(r_prof, blockNumR,
                                                   blockSizeR,
                                                   params["noiseR"])

        # фильтрация боковых лепестков
        for smp_ref in range(params["minRbin"], params["maxRbin"]):
            # отсчёт является локальным максимумом и его амплитуда больше 
            # порогового значенияна профиле дальности
            if (r_prof[smp_ref] >  r_thresh[vel_ref]) and  \
               (r_prof[smp_ref] >= r_prof[smp_ref-1]) and \
               (r_prof[smp_ref] >= r_prof[smp_ref+1]):
                # оценка уровня шума на профиле скорости
                v_prof = rvacc[:, smp_ref]
                if (v_mask[smp_ref] == 0):
                    v_thresh[smp_ref] = threshEstimationMedian(v_prof, blockNumV, 
                                                                blockSizeV, params["noiseV"])
                    # отмечаем в маске дальность, для которой выполнена оценка уровня шума
                    v_mask[smp_ref] = 1

                # отсчёт является локальным максимумом и его амплитуда больше 
                # порогового значения на профиле дальности
                if (r_prof[smp_ref] > v_thresh[smp_ref]) and  \
                   (r_prof[smp_ref] >= v_prof[vel_ref-1]) and \
                   (r_prof[smp_ref] >= v_prof[vel_ref+1]):
                    pnts.append([vel_ref, smp_ref])

    return np.array(pnts)


def threshEstimationCFAR(smp, prof, num_train, num_guard, noise_scale):

    num_cells = prof.size
    num_train_half = num_train >> 1
    num_guard_half = num_guard >> 1

    noise = 0.0
    for ref in range (0, num_train_half):
        # номера отсчётов с учётом периодичности спектра (всегда в нужном диапазоне значений)
        right_cell = (smp + num_guard_half + 1 + ref) % num_cells
        left_cell  = (smp + num_cells - num_guard_half - 1 - ref) % num_cells
        noise += prof[right_cell] + prof[left_cell]

    return (noise_scale*noise/num_train)