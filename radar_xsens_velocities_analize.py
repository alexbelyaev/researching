#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 29 11:30:43 2022

@author: alex-stend
"""
import numpy as np
import glob
import os
import matplotlib.pyplot as plt
import time
from tqdm import tqdm
# import processingFunctions as pf

base_route = '/home/alex-stend/datasets/new_xsens_datasets/'

radar_timestamps = np.loadtxt(base_route + 'bags/xsens_velocs3_radar.txt')
xsens_timestamps = np.loadtxt(base_route + 'bags/xsens_velocs3_xsens.txt')

radar_frames = sorted(glob.glob(base_route + '/bags/radar/*.npy'))
xsens_frames = sorted(glob.glob(base_route + '/bags/xsens/*.npy'))

def find_nearest(array, value):

    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

cur_fr = 0
real_vels_hist = []

cnt = 0
end_frame = 250

filter_case = 0 # 0 - range, 1 - azimuth, 2 - elevation

# исследуемые пределы по дальности:
min_range = 0
max_range = 40
# исследуемые пределы по азимуту:
min_azim = 0
max_azim = 10
# исследуемые пределы по углу места:
min_elev = 25
max_elev = 30

for i in tqdm(range(cur_fr, cur_fr + end_frame)):
    radar_pointcloud = np.load(radar_frames[i])
    
    xsens_idx = find_nearest(xsens_timestamps, radar_timestamps[i] - 0e6)
    xsens_xyz = np.load(xsens_frames[xsens_idx])


    if (end_frame == 1):
        fig = plt.figure()
        ax = fig.add_subplot(111)
    
        plt.plot(-radar_pointcloud[:, 1], radar_pointcloud[:, 0],
                "ro", markersize=4, alpha=0.9, color="green")
        plt.ylim([0,50])
        plt.xlim([-17.5,17.5])

    az_tg = []
    el_tg = []
    
    for i in range(len(radar_pointcloud)):
        # подсчёт азимутов и углов места всех целей на анализируемом кадре
        x = radar_pointcloud[i][0]
        y = radar_pointcloud[i][1]
        z = radar_pointcloud[i][2]
        az_tg.append(np.arctan(y/x)*57)
        el_tg.append(np.arctan(z/x)*57)
        
    real_vels = []
    
    for i in range(len(radar_pointcloud)):
        # фильтрация по дальности
        if (filter_case == 0):
            # ограничения по азимуту сверху/снизу
            if (np.sqrt(radar_pointcloud[i][0]**2 + radar_pointcloud[i][1]**2)
                > min_range):
                if (np.sqrt(radar_pointcloud[i][0]**2
                            + radar_pointcloud[i][1]**2) < max_range):
                    # расчёт собственной компенсированной скорости цели
                    real_vels.append(radar_pointcloud[i][3]/np.cos(az_tg[i]/57)/np.cos(el_tg[i]/57)
                                     + np.sqrt(xsens_xyz[0]**2
                                               + xsens_xyz[1]**2))
                    # подсчёт количества точек со скоростью в районе нуля
                    if (radar_pointcloud[i][3]/np.cos(az_tg[i]/57) 
                        + np.sqrt(xsens_xyz[0]**2 + xsens_xyz[1]**2
                                  + xsens_xyz[2]**2) > -0.1):
                        if (radar_pointcloud[i][3]/np.cos(az_tg[i]/57)
                            + np.sqrt(xsens_xyz[0]**2 + xsens_xyz[1]**2
                                      + xsens_xyz[2]**2) < 0.1):
                            cnt += 1
        # фильтрация по азимуту
        if (filter_case == 1):
            # ограничения по азимуту сверху/снизу
            if (abs(az_tg[i]) < max_azim):
                if (abs(az_tg[i]) > min_azim):
                    # расчёт собственной компенсированной скорости цели
                    real_vels.append(radar_pointcloud[i][3]/np.cos(az_tg[i]/57)   
                                     + np.sqrt(xsens_xyz[0]**2
                                               + xsens_xyz[1]**2))
                    # подсчёт количества точек со скоростью в районе нуля
                    if (radar_pointcloud[i][3]/np.cos(az_tg[i]/57) 
                        + np.sqrt(xsens_xyz[0]**2 + xsens_xyz[1]**2
                                  + xsens_xyz[2]**2) > -0.1):
                        if (radar_pointcloud[i][3]/np.cos(az_tg[i]/57) 
                            + np.sqrt(xsens_xyz[0]**2 + xsens_xyz[1]**2
                                      + xsens_xyz[2]**2) < 0.1):
                            cnt += 1
        # фильтрация по углу места      
        if (filter_case == 2):
            # ограничения по углу места сверху/снизу
            if (abs(el_tg[i]) < max_elev):
                if (abs(el_tg[i]) > min_elev):
                    # расчёт собственной компенсированной скорости цели
                    real_vels.append(radar_pointcloud[i][3]/np.cos(az_tg[i]/57)   
                                     + np.sqrt(xsens_xyz[0]**2
                                               + xsens_xyz[1]**2))
                    # подсчёт количества точек со скоростью в районе нуля
                    if (radar_pointcloud[i][3]/np.cos(az_tg[i]/57) 
                        + np.sqrt(xsens_xyz[0]**2 + xsens_xyz[1]**2
                                  + xsens_xyz[2]**2) > -0.1):
                        if (radar_pointcloud[i][3]/np.cos(az_tg[i]/57) 
                            + np.sqrt(xsens_xyz[0]**2 + xsens_xyz[1]**2
                                      + xsens_xyz[2]**2) < 0.1):
                            cnt += 1
        if (end_frame == 1):
            ann_vel = "%.3fм/с" % real_vels[i]
            plt.annotate(ann_vel, (-radar_pointcloud[i][1]+0.5,
                                   radar_pointcloud[i][0]+0.5), size=10)
            ann_ampl = '%.2E' % radar_pointcloud[i][4]  
            plt.annotate(ann_ampl, (-radar_pointcloud[i][1]+1.25,
                                    radar_pointcloud[i][0]+0.5), size=10)
    real_vels_hist.extend(real_vels)

plt.figure()
plt.hist(real_vels_hist, 200)
plt.xlim([-2, 2])

if (filter_case == 0):
    plt.title(f"range from %d m " % min_range + f"to %d m" % max_range)
    plt.savefig(f'velocities_histogtrams_pics/range_velocities_hist_%d'
                % max_range + '.png')
if (filter_case == 1):
    plt.title(f"azimuth from %d grad " % min_azim + f"to %d grad" % max_azim)
    plt.savefig(f'velocities_histogtrams_pics/azimuths_velocities_hist_%d'
                % max_azim + '.png')
if (filter_case == 2):
    plt.title(f"elevation from %d grad " % min_elev + f"to %d grad" % max_elev)
    plt.savefig(f'velocities_histogtrams_pics/elevations_velocities_hist_%d'
                % max_elev + '.png')

###############################################################################
###########   расчёт систематической ошибки при измерении скорости   ##########
###############################################################################

c = 299792458.0

f0 = 77e9
f1 = 77.7e9

lmb0 = c/f0
lmb1 = c/f1

ti = 8e-6
t_ramp_end = 22e-6
tp = (ti + t_ramp_end)*8

vmax0 = lmb0/(4*tp)
vmax1 = lmb1/(4*tp)

vel_grid0 = np.arange(-vmax0, vmax0, 2*vmax0/256)
vel_grid1 = np.arange(-vmax1, vmax1, 2*vmax1/256)

real_vel = 20

dfi0 = 4*np.pi*real_vel*tp/lmb0
dfi1 = 4*np.pi*real_vel*tp/lmb1

v0 = lmb0*dfi1/(4*np.pi*tp)
v1 = lmb1*dfi1/(4*np.pi*tp)

syst_err = abs(v0 - v1)
print("velocity estimation's systematic error is %f" % syst_err)