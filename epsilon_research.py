#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 12 17:23:02 2022

@author: alex-stend
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d

min_limit_r = -1.5
max_limit_r = 1.5
N = 64

vmax = 15
min_limit_v = -vmax
max_limit_v = vmax
M = 128

R = np.linspace(min_limit_r, max_limit_r, N)
V = np.linspace(min_limit_v, max_limit_v, M)

r_fix = 2.5
# for i in range(20):
#     v_var = np.sqrt((i+1)*r_fix + V**2)

#     # plt.figure()
#     plt.plot(V, v_var, label="diff range = " + str((i+1)*r_fix))
#     plt.legend(loc="upper left")


r, v = np.meshgrid(R, V) 
eps = np.sqrt(r ** 2 + v ** 2)
    
#create 3d axes
fig = plt.figure()
ax = plt.axes(projection='3d')
ax.plot_surface(r, v, eps, rstride=1, cstride=1,
                cmap='viridis')
plt.show()
ax.set_zlim(0,2)


# for i in range(M):
#     for j in range(N):
#         if (z[i, j] < 2)