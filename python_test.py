# -*- coding: utf-8 -*-
"""
Created on Mon Apr 11 10:52:02 2022

@author: Belyaev
"""
import numpy as np
import processingFunctions as pf
import matplotlib.pyplot as plt

# radar_model = str('i1A0000')

# radar_model_ = np.chararray(16, itemsize=7)

# itemsize = len(radar_model)
# for i in range(itemsize):
#     radar_model_[i] = radar_model[i]

N = 128
M = 256

###############################################################################
###############################################################################
###############################################################################

test_array_mimo = abs(np.random.normal(0, 1e4, [M, N]))
test_array_mimo_cfar = np.zeros([M, N], int)

ampl_min = 5e4
ampl_max = 1e5

m0_mimo = 10
n0_mimo = 88

m1_mimo = 25
n1_mimo = 110

m2_mimo = 40
n2_mimo = 120

m3_mimo = 55
n3_mimo = 124

m4_mimo = 76
n4_mimo = 0

m5_mimo = 100
n5_mimo = 3

m6_mimo = 140
n6_mimo = 5

###############################################################################

test_array_mimo[m0_mimo-1, n0_mimo] = ampl_min
test_array_mimo[m0_mimo, n0_mimo-1] = ampl_min
test_array_mimo[m0_mimo, n0_mimo] = ampl_max
test_array_mimo[m0_mimo+1, n0_mimo] = ampl_min
test_array_mimo[m0_mimo, n0_mimo+1] = ampl_min

###############################################################################

test_array_mimo[m1_mimo-1, n1_mimo] = ampl_min
test_array_mimo[m1_mimo, n1_mimo-1] = ampl_min
test_array_mimo[m1_mimo, n1_mimo] = ampl_max
test_array_mimo[m1_mimo+1, n1_mimo] = ampl_min
test_array_mimo[m1_mimo, n1_mimo+1] = ampl_min

###############################################################################

test_array_mimo[m2_mimo-1, n2_mimo] = ampl_min
test_array_mimo[m2_mimo, n2_mimo-1] = ampl_min
test_array_mimo[m2_mimo, n2_mimo] = ampl_max
test_array_mimo[m2_mimo+1, n2_mimo] = ampl_min
test_array_mimo[m2_mimo, n2_mimo+1] = ampl_min

###############################################################################

test_array_mimo[m3_mimo-1, n3_mimo] = ampl_min
test_array_mimo[m3_mimo, n3_mimo-1] = ampl_min
test_array_mimo[m3_mimo, n3_mimo] = ampl_max
test_array_mimo[m3_mimo+1, n3_mimo] = ampl_min
test_array_mimo[m3_mimo, n3_mimo+1] = ampl_min

###############################################################################

test_array_mimo[m4_mimo-1, n4_mimo] = ampl_min
test_array_mimo[m4_mimo, n4_mimo-1] = ampl_min
test_array_mimo[m4_mimo, n4_mimo] = ampl_max
test_array_mimo[m4_mimo+1, n4_mimo] = ampl_min
test_array_mimo[m4_mimo, n4_mimo+1] = ampl_min

###############################################################################

test_array_mimo[m5_mimo-1, n5_mimo] = ampl_min
test_array_mimo[m5_mimo, n5_mimo-1] = ampl_min
test_array_mimo[m5_mimo, n5_mimo] = ampl_max
test_array_mimo[m5_mimo+1, n5_mimo] = ampl_min
test_array_mimo[m5_mimo, n5_mimo+1] = ampl_min

###############################################################################

test_array_mimo[m6_mimo-1, n6_mimo] = ampl_min
test_array_mimo[m6_mimo, n6_mimo-1] = ampl_min
test_array_mimo[m6_mimo, n6_mimo] = ampl_max
test_array_mimo[m6_mimo+1, n6_mimo] = ampl_min
test_array_mimo[m6_mimo, n6_mimo+1] = ampl_min

###############################################################################

false_alarm_rate_mimo = 1e-2

cfar_prms = {'num_train': (6, 6), 'num_guard': (2, 2),
             'fa_rate': false_alarm_rate_mimo,
             'Rfa_rate': 15e-2, 'Dfa_rate': 24e-2}

peak_idx_mimo = pf.pref_sums_cfar(test_array_mimo, cfar_prms)
test_array_mimo_cfar[peak_idx_mimo] = 1

plt.figure()
plt.imshow(test_array_mimo)
plt.title("mimo gen map")

plt.figure()
plt.imshow(test_array_mimo_cfar)
plt.title("mimo cfar map")

###############################################################################
###############################################################################

test_array_one_tx = abs(np.random.normal(0, 1e4, [M, N]))
test_array_one_tx_cfar = np.zeros([M, N], int)

ampl_min = 5e4
ampl_max = 1e5

m0_one_tx = 10
n0_one_tx = 70

m1_one_tx = 25
n1_one_tx = 75

m2_one_tx = 40
n2_one_tx = 78

m3_one_tx = 55
n3_one_tx = 79

m4_one_tx = 76
n4_one_tx = 80

m5_one_tx = 100
n5_one_tx = 81

m6_one_tx = 140
n6_one_tx = 82

###############################################################################

test_array_one_tx[m0_one_tx-1, n0_one_tx] = ampl_min
test_array_one_tx[m0_one_tx, n0_one_tx-1] = ampl_min
test_array_one_tx[m0_one_tx, n0_one_tx] = ampl_max
test_array_one_tx[m0_one_tx+1, n0_one_tx] = ampl_min
test_array_one_tx[m0_one_tx, n0_one_tx+1] = ampl_min

###############################################################################

test_array_one_tx[m1_one_tx-1, n1_one_tx] = ampl_min
test_array_one_tx[m1_one_tx, n1_one_tx-1] = ampl_min
test_array_one_tx[m1_one_tx, n1_one_tx] = ampl_max
test_array_one_tx[m1_one_tx+1, n1_one_tx] = ampl_min
test_array_one_tx[m1_one_tx, n1_one_tx+1] = ampl_min

###############################################################################

test_array_one_tx[m2_one_tx-1, n2_one_tx] = ampl_min
test_array_one_tx[m2_one_tx, n2_one_tx-1] = ampl_min
test_array_one_tx[m2_one_tx, n2_one_tx] = ampl_max
test_array_one_tx[m2_one_tx+1, n2_one_tx] = ampl_min
test_array_one_tx[m2_one_tx, n2_one_tx+1] = ampl_min

###############################################################################

test_array_one_tx[m3_one_tx-1, n3_one_tx] = ampl_min
test_array_one_tx[m3_one_tx, n3_one_tx-1] = ampl_min
test_array_one_tx[m3_one_tx, n3_one_tx] = ampl_max
test_array_one_tx[m3_one_tx+1, n3_one_tx] = ampl_min
test_array_one_tx[m3_one_tx, n3_one_tx+1] = ampl_min

###############################################################################

test_array_one_tx[m4_one_tx-1, n4_one_tx] = ampl_min
test_array_one_tx[m4_one_tx, n4_one_tx-1] = ampl_min
test_array_one_tx[m4_one_tx, n4_one_tx] = ampl_max
test_array_one_tx[m4_one_tx+1, n4_one_tx] = ampl_min
test_array_one_tx[m4_one_tx, n4_one_tx+1] = ampl_min

###############################################################################

test_array_one_tx[m5_one_tx-1, n5_one_tx] = ampl_min
test_array_one_tx[m5_one_tx, n5_one_tx-1] = ampl_min
test_array_one_tx[m5_one_tx, n5_one_tx] = ampl_max
test_array_one_tx[m5_one_tx+1, n5_one_tx] = ampl_min
test_array_one_tx[m5_one_tx, n5_one_tx+1] = ampl_min

###############################################################################

test_array_one_tx[m6_one_tx-1, n6_one_tx] = ampl_min
test_array_one_tx[m6_one_tx, n6_one_tx-1] = ampl_min
test_array_one_tx[m6_one_tx, n6_one_tx] = ampl_max
test_array_one_tx[m6_one_tx+1, n6_one_tx] = ampl_min
test_array_one_tx[m6_one_tx, n6_one_tx+1] = ampl_min

###############################################################################

false_alarm_rate_mimo = 1e-2

cfar_prms = {'num_train': (4, 4), 'num_guard': (2, 2),
             'fa_rate': false_alarm_rate_mimo,
             'Rfa_rate': 15e-2, 'Dfa_rate': 24e-2}

peak_idx_one_tx = pf.pref_sums_cfar(test_array_one_tx, cfar_prms)
test_array_one_tx_cfar[peak_idx_one_tx] = 1

plt.figure()
plt.imshow(test_array_one_tx)
plt.title("one_tx gen map")

plt.figure()
plt.imshow(test_array_one_tx_cfar)
plt.title("one_tx cfar map")


###############################################################################
###############################################################################

c = 299792458.0
f = 77e9
lmb = c/f
d = lmb/2

tx_num = 4
fd = 5e6
ti = 8e-6
t_ramp_end = 22e-6
S = 33e12
tp_mimo = (ti + t_ramp_end)*tx_num
tp_one_tx = (ti + t_ramp_end)

dbins_num = 128
vmax_mimo = 3600*lmb/(4*tp_mimo)/1000
real_velocs_mimo = (2 * peak_idx_mimo[1] * vmax_mimo) / dbins_num - vmax_mimo

vmax_one_tx = 3600*lmb/(4*tp_one_tx)/1000
real_velocs_one_tx = \
    (2 * peak_idx_one_tx[1] * vmax_one_tx) / dbins_num - vmax_one_tx

one_tx_to_mimo_velocities = np.zeros(len(real_velocs_one_tx), int)

for i in range(len(real_velocs_one_tx)):
    if (real_velocs_one_tx[i] <= vmax_mimo):
        one_tx_to_mimo_velocities[i] = \
            (real_velocs_one_tx[i] + vmax_mimo)*dbins_num/(2*vmax_mimo)
    else:
        if (real_velocs_one_tx[i] < 0):
            new_veloc_est = 2*vmax_mimo + real_velocs_one_tx[i]
        else:
            new_veloc_est = -2*vmax_mimo + real_velocs_one_tx[i]
        one_tx_to_mimo_velocities[i] = \
            (new_veloc_est + vmax_mimo)*dbins_num/(2*vmax_mimo)

peak_idx_one_tx_to_mimo = [peak_idx_one_tx[0], one_tx_to_mimo_velocities]

test_array_one_tx_to_mimo = np.zeros([M, N], int)
test_array_one_tx_to_mimo[peak_idx_one_tx_to_mimo] = 1

plt.figure()
plt.imshow(test_array_one_tx_to_mimo)
plt.title("one_tx_to_mimo map")

###############################################################################

veloc_win = 3
range_win = 2

# num_cells = test_array_one_tx_to_mimo.shape

# test_array_one_tx_to_mimo = \
#     np.concatenate((test_array_one_tx_to_mimo[num_cells[0] - range_win::, :],
#                     test_array_one_tx_to_mimo,
#                     test_array_one_tx_to_mimo[0:range_win, :]), axis=0)
# test_array_one_tx_to_mimo = \
#     np.concatenate((test_array_one_tx_to_mimo[:, num_cells[1] - veloc_win::],
#                     test_array_one_tx_to_mimo,
#                     test_array_one_tx_to_mimo[:, 0:veloc_win]), axis=1)

# test_array_mimo_cfar = \
#     np.concatenate((test_array_mimo_cfar[num_cells[0] - range_win::, :],
#                     test_array_mimo_cfar,
#                     test_array_mimo_cfar[0:range_win, :]), axis=0)
# test_array_mimo_cfar = \
#     np.concatenate((test_array_mimo_cfar[:, num_cells[1] - veloc_win::],
#                     test_array_mimo_cfar,
#                     test_array_mimo_cfar[:, 0:veloc_win]), axis=1)


test_result_map = np.zeros([M, N], int)

# for i in range(range_win, M + range_win):
#     for j in range(range_win, N + range_win):
#         for n in range(-veloc_win, veloc_win+1):
#             for m in range(-range_win, range_win+1):
#                 if (test_array_mimo_cfar[i,j] > 0):
#                     if (test_array_one_tx_to_mimo[i+m, j+n] == \
#                             test_array_mimo_cfar[i, j]):
#                         test_result_map[i-range_win, j-veloc_win] = 1

targs_info = {'R_index': [],
              'V_index': [],
              'real_velocity': [],
              }

for i in range(len(peak_idx_one_tx_to_mimo[0])):
    r = peak_idx_one_tx_to_mimo[0][i]
    v = peak_idx_one_tx_to_mimo[1][i]
    print(r)
    print(v)
    print(test_array_one_tx_to_mimo[r, v])
    if ((v + veloc_win < dbins_num) and (v - veloc_win >= 0)):
        for n in range(-veloc_win, veloc_win+1):
            for m in range(-range_win, range_win+1):
                # if (test_array_mimo_cfar[r, v] > 0):
                    if (test_array_one_tx_to_mimo[r, v] == \
                            test_array_mimo_cfar[r+m, v+n]):
                        targs_info['R_index'].append(r + m)
                        targs_info['V_index'].append(v + n)
                        targs_info['real_velocity'].append(real_velocs_one_tx[i])
                        test_result_map[r + m, v + n] = 1

    if (v + veloc_win > dbins_num):
        diff = v + veloc_win - dbins_num
        for n in range(-veloc_win, veloc_win - diff):
            for m in range(-range_win, range_win+1):
                if (test_array_one_tx_to_mimo[r, v] == \
                        test_array_mimo_cfar[r+m, v+n]):
                    targs_info['R_index'].append(r + m)
                    targs_info['V_index'].append(v + n)
                    targs_info['real_velocity'].append(real_velocs_one_tx[i])
                    test_result_map[r + m, v + n] = 1
        for n in range(0, diff):
            for m in range(-range_win, range_win+1):
                if (test_array_one_tx_to_mimo[r, v] == \
                        test_array_mimo_cfar[r+m, n]):
                    targs_info['R_index'].append(r + m)
                    targs_info['V_index'].append(v + n)
                    targs_info['real_velocity'].append(real_velocs_one_tx[i])
                    test_result_map[r + m, n] = 1

    if (v - veloc_win < 0):
        diff = veloc_win - v
        for n in range(-veloc_win + diff, veloc_win):
            for m in range(-range_win, range_win+1):
                if (test_array_one_tx_to_mimo[r, v] == \
                        test_array_mimo_cfar[r+m, v+n]):
                    targs_info['R_index'].append(r + m)
                    targs_info['V_index'].append(v + n)
                    targs_info['real_velocity'].append(real_velocs_one_tx[i])
                    test_result_map[r + m, v + n] = 1
        for n in range(dbins_num - diff, dbins_num):
            for m in range(-range_win, range_win+1):
                if (test_array_one_tx_to_mimo[r, v] == \
                        test_array_mimo_cfar[r+m, n]):
                    targs_info['R_index'].append(r + m)
                    targs_info['V_index'].append(v + n)
                    targs_info['real_velocity'].append(real_velocs_one_tx[i])
                    test_result_map[r + m, n] = 1


plt.figure()
plt.imshow(test_result_map)
plt.title("test_result_map map")
