#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 25 13:50:45 2022

@author: alex-stend
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy import constants
import processingFunctions as pf
import glob
# plt.close('all')

sizeParams = {'smp_num': 256,
              'pls_num': 32,
              'rx_num': 4, 'tx_num': 8,
              'tx_num_az': 2,
              'rbins_num': 256,
              'dbins_num': 64,
              'abins_num': 256, 'rxq_num': ''}
sizeParams['rxq_num'] = 64

c = 299792458.0
f = 77e9
lmb = c/f
d = lmb/2

fd = 18.75e6
ti = 8e-6
t_ramp_end = 22e-6
tp = (ti + t_ramp_end)*8
S = 50e12



# Приведение отчётов к дальности, м
f_range = np.zeros(sizeParams['rbins_num'])
for smp_ref in range(0, sizeParams['rbins_num']):
    f_range[smp_ref] = (smp_ref / sizeParams['rbins_num']) * fd
# Преобразование частот в дальность
ranges = ((constants.speed_of_light * f_range * tp) / (2 * (S * tp)))

f_ang = np.fft.fftfreq(sizeParams['abins_num'], d)
# Преобразование частот в азимут
angles = np.rad2deg(np.arcsin(f_ang * lmb))
angles = np.fft.fftshift(angles[:]*1)

smp_num = sizeParams['smp_num']
pls_num = sizeParams['pls_num']
rxq_num = sizeParams['rxq_num']
rx_phys = sizeParams['rx_num']
tx_phys = sizeParams['tx_num']

count = smp_num*pls_num*rxq_num*2

calib_koefs = np.load('clb_03.npz')


num_of_frame = 0

# число 16-битных отсчётов в кадре (множитель 2 обусловлен комплексными данными)
frm_s  = 2*smp_num*pls_num*rx_phys*tx_phys * 4  #(4 awr data in one file)
# размер кадра в байтах (множитель 2 обусловлен 16-битными данными)
frm_w  = 2*frm_s
frame = 4

path = '/home/alex-stend/datasets/velocs_datasets_30_05_2022/bags/'

data = open(path + 'radar/raw_test_3sf_kb_calibr.bin', 'rb')
position = frame*frm_w
data.seek(position, 0)
Xt = np.fromfile(data, dtype = np.dtype('int16'), count = frm_s)


radar_timestamp = np.loadtxt(path + 'radar/raw_test_3sf_kb_calibr.txt')
lidar_timestamp = np.loadtxt(path + 'lidar/raw_test_3sf_kb_calibr.txt')
lidar_frames = sorted(glob.glob(path + 'lidar/*.npy'))

Xt_I = Xt[::2]
Xt_Q = Xt[1::2]
Xt = Xt_I + 1j*Xt_Q

frm_sc = smp_num*pls_num*rx_phys*tx_phys  # количество комплексных отсчётов
                                          # для одного чипа
dataCube_badslave = np.reshape(Xt[: frm_sc],
                               (pls_num, tx_phys, rx_phys, smp_num))
dataCube_goodslave = np.reshape(Xt[frm_sc:2*frm_sc],
                                (pls_num, tx_phys, rx_phys, smp_num))
dataCube_master = np.reshape(Xt[2*frm_sc:3*frm_sc],
                             (pls_num, tx_phys, rx_phys, smp_num))
dataCube_uglyslave = np.reshape(Xt[3*frm_sc:4*frm_sc],
                                (pls_num, tx_phys, rx_phys, smp_num))

Xtbad16 = np.concatenate((dataCube_badslave[:, 0, :, :],
                          dataCube_badslave[:, 1, :, :],
                          dataCube_badslave[:, 2, :, :],
                          dataCube_badslave[:, 3, :, :]),
                         axis=1)

Xtmaster16 = np.concatenate((dataCube_master[:, 0, :, :],
                             dataCube_master[:, 1, :, :],
                             dataCube_master[:, 2, :, :],
                             dataCube_master[:, 3, :, :]),
                            axis=1)

Xtugly16 = np.concatenate((dataCube_uglyslave[:, 0, :, :],
                           dataCube_uglyslave[:, 1, :, :],
                           dataCube_uglyslave[:, 2, :, :],
                           dataCube_uglyslave[:, 3, :, :]),
                          axis=1)

Xtgood16 = np.concatenate((dataCube_goodslave[:, 0, :, :],
                           dataCube_goodslave[:, 1, :, :],
                           dataCube_goodslave[:, 2, :, :],
                           dataCube_goodslave[:, 3, :, :]),
                          axis=1)

Xt64 = np.concatenate((Xtbad16, Xtgood16, Xtmaster16, Xtugly16), axis=1)
dataCube = np.transpose(Xt64, (0, 2, 1))


# калибровка - перемножение каждого набора отсчётов по виртуальным приёмникам
# на калибровочный вектор calib_koefs
dataCube = dataCube*calib_koefs['clba']


# выполнение пунктов согласно структурной схеме

# 1. Расчёт RV-карты

sizeParams_cascade = sizeParams
sizeParams_cascade['rxq_num'] = 64
RV_map, RVcube = pf.rvMapProc(dataCube, sizeParams_cascade)

RV_map[:, :int(sizeParams['smp_num']*0.02)] = 0
RV_map[:, int(sizeParams['smp_num']*0.98):] = 0

## 2. Peaks Detector для полученной RV-карты
false_alarm_rate_mimo = 1e-1  # 3e-4 - исходный
cfarPrms = {'num_train': (13, 13), 'num_guard': (9, 9),
            'fa_rate': false_alarm_rate_mimo,
            'Rfa_rate': 15e-2, 'Dfa_rate': 24e-2}
peak_idx = pf.twoDimsCFAR(RV_map, cfarPrms)
peak_idx = np.array(peak_idx, dtype=int).T
pntsMagRV = np.zeros([sizeParams['dbins_num'],
                      sizeParams['rbins_num']], dtype=float)
pntsRange = peak_idx[:, 1]
pntsVel = peak_idx[:, 0]
pntsMagRV[pntsVel, pntsRange] = 1

# plt.plot(pntsRange, pntsVel, "o", markersize=5, alpha=0.9, color="green")

# 3. Расчёт RА-карт на найденных скоростях. Результат - RAcubeCascade.
# 4. Расчёт суммарной RA-карты. Результат - ra_map_cascade.
#sizeParams['non_zero_velocs'] = pntsVel
sizeParams['range_beta'] = 10
sizeParams['azims_beta'] = 15
ra_map_cascade, RAcubeCascade = pf.raMapBuilding(dataCube, sizeParams)
ra_map_cascade[:int(sizeParams['smp_num']*0.02), :] = 0
ra_map_cascade[int(sizeParams['smp_num']*0.98):, :] = 0


pd_ra_map = np.zeros(np.shape(ra_map_cascade), int)
nAzims = 256

for i in range(256):
    pntsAzmFFT = np.array([], dtype=int)
    azims = ra_map_cascade[i]
    edge = 2
    max_lvl = 3.8
    azimProfileAcc = np.sum(azims[edge:nAzims-edge])
    profile_max_lvl = azimProfileAcc*max_lvl/float(nAzims - 2*edge)

    prof_d = azims[1:].copy() - azims[:nAzims-1]
    prof_d = np.append(prof_d[0], prof_d)

    for ref in range(edge, nAzims-edge):
        if ((prof_d[ref] >= 0) and (prof_d[ref+1] <= 0)):
            if (azims[ref] > profile_max_lvl):
                newMag = azims[ref]

                pntsAzmFFT = np.append(pntsAzmFFT, ref)

    pd_ra_map[i, pntsAzmFFT] = 1

R, A = np.nonzero(pd_ra_map)

plt.figure()
X, Y = np.meshgrid(angles, ranges)
plt.pcolormesh(Y*np.sin(np.radians(X)), Y*np.cos(np.radians(X)),
                (ra_map_cascade/np.max(ra_map_cascade)), 
                cmap=plt.cm.get_cmap('jet'), vmax = 0.07)

plt.colorbar()
plt.ylabel('Y-координата, м')
plt.xlabel('X-координата, м')
plt.ylim([0, 31.5])
plt.xlim([-30, 30])
plt.gca().set_aspect('equal', adjustable='box')
plt.show()

masPntsMagRA = {'elems':  pd_ra_map[np.nonzero(pd_ra_map)],
                'Xindex': R,
                'Yindex': A,
                }

Xnew, Ynew = Y*np.sin(np.radians(X)), Y*np.cos(np.radians(X))
Xtargs = Xnew[masPntsMagRA['Xindex'], masPntsMagRA['Yindex']]
Ytargs = Ynew[masPntsMagRA['Xindex'], masPntsMagRA['Yindex']]

Rtargs = Y[masPntsMagRA['Xindex'], masPntsMagRA['Yindex']]
Atargs = X[masPntsMagRA['Xindex'], masPntsMagRA['Yindex']]

plt.plot(Xtargs, Ytargs, "x", markersize=6, alpha=0.9, color="blue")


###############################################################################
sizeParams_cascade = sizeParams
sizeParams_cascade['rxq_num'] = 64
RV_map, RVcube = pf.rvMapProc(dataCube, sizeParams_cascade)

RV_map[:, :int(sizeParams['smp_num']*0.02)] = 0
RV_map[:, int(sizeParams['smp_num']*0.98):] = 0


pd_rv_map = np.zeros(np.shape(RV_map), int)
nAzims = 256

lidar_idx = pf.find_nearest(lidar_timestamp, radar_timestamp[frame])
lidar_xyz = np.load(lidar_frames[lidar_idx])
plt.plot(lidar_xyz[:, 1], -lidar_xyz[:, 0], "ro", markersize=1,  color="pink")

